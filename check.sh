
DEPLOY1_FILE="/mnt/deploy1/xyz.txt"
DEPLOY2_FILE="/tmp/abc.tmp"
DEPLOY3_FILE="/var/config/svc-config.txt"


function check_deploy(){
	if [[ -f $DEPLOY1_FILE ]]; then
		echo "$DEPLOY1_FILE deployed successfully!!!"
	fi

	if [[ -f $DEPLOY2_FILE ]]; then
		echo "$DEPLOY2_FILE deployed successfully!!!"
	fi

	if [[ -f $DEPLOY3_FILE ]]; then
		echo "$DEPLOY3_FILE deployed successfully!!!"
	fi
}

check_deploy