#!/usr/bin/env bash

#title           :hourly.sh
#description     :This script will execute hourly via cron to deploy config files
#author          :Derrick Lord
#date            :20190909
#version         :0.1   
#usage           :./hourly.sh
#notes           :Script requires entry in sudoers to restrict password prompt
#notes           :[username]     ALL = (ALL) NOPASSWD:ALL
#notes           :[username] should have R/W access to the following directories:
#notes           :    1. /data1/deploy
#notes           :    2. /mnt/deploy
#notes           :    3. /var/config
#bash_version    :GNU bash 3.2.57(1)-release
#==============================================================================

#Set PATH so cron can find executables (mv, unzip, date)
PATH=/usr/local/bin:/usr/local/sbin:~/bin:/usr/bin:/bin:/usr/sbin:/sbin

#Enable clean up of /data1/deploy directory (true/false)
ENABLE_CLEANUP=true

#Enable logging to deploy.log in /tmp directory
echo $(date) #Log System start time
cd /data1/deploy

#Setup script configuration variables
DEPLOY_FILES="/data1/deploy/*.zip"

DEPLOY1_SRC="/data1/deploy/xyz.txt"
DEPLOY2_SRC="/data1/deploy/abc.tmp"
DEPLOY3_SRC="/data1/deploy/svc-config.txt"

DEPLOY1_DEST="/mnt/deploy1/xyz.txt"
DEPLOY2_DEST="/tmp/abc.tmp"
DEPLOY3_DEST="/var/config/svc-config.txt"

#Function: Check for Files to Deploy in /data1/deploy
function check_for_deploy_files(){
	files=`ls $DEPLOY_FILES`
	for file in $files; do
		if [[ -f $file ]]; then
			echo "Unzipping deployent file $file" 
			unzip_file $file
		else
			echo "No Deployment file found" >&2 #Log failure to stderr
		fi
	done
	deploy_files
}

#Function: Unzip archive into current directory
#Arguments: $1 == zip archive filename
function unzip_file(){
	echo "Unzipping Filename: $1"
	(sudo unzip -q $1)
}

#Function: Deploy unzipped files to specific target directories
#Notes: Will overwrite currently deployed files
function deploy_files(){
	if [[ -f $DEPLOY1_SRC ]]; then
			echo "Deploying $DEPLOY1_SRC file..."
			(sudo mv -f $DEPLOY1_SRC $DEPLOY1_DEST)
	fi

	if [[ -f $DEPLOY2_SRC ]]; then
			echo "Deploying $DEPLOY2_SRC file..."
			(sudo mv -f $DEPLOY2_SRC $DEPLOY2_DEST)
	fi

	if [[ -f $DEPLOY3_SRC ]]; then
			echo "Deploying $DEPLOY3_SRC file..."
			(sudo mv -f $DEPLOY3_SRC $DEPLOY3_DEST)
	fi
}

#Function: Verify Successful deployment of files
#Notes: Will log unsuccessfully deployed files to deploy.log
function check_deploy_success(){
	if [[ -f $DEPLOY1_DEST ]]; then
		echo "$DEPLOY1_DEST deployment successfully!!!"
	else
		echo "$DEPLOY1_DEST deployment failed!!!" >&2 #Log failure to stderr
	fi

	if [[ -f $DEPLOY2_DEST ]]; then
		echo "$DEPLOY2_DEST deployment successfully!!!"
	else
		echo "$DEPLOY2_DEST deployment failed!!!" >&2 #Log failure to stderr
	fi

	if [[ -f $DEPLOY3_DEST ]]; then
		echo "$DEPLOY3_DEST deployment successfully!!!"
	else
		echo "$DEPLOY3_DEST deployment failed!!!" >&2 #Log failure to stderr
	fi
}

#Function: Clean up /data1/deploy directory for next hourly deployment
function clean_up_deploy(){
	if [ "$ENABLE_CLEANUP" = true ]; then
		echo "Removing zip archive from /data1/deploy directory..."
		(sudo rm /data1/deploy/*.zip)
	fi
}



function main(){
	check_for_deploy_files
	check_deploy_success
	clean_up_deploy
}

main
echo $(date) #Log System end time
exit 0
