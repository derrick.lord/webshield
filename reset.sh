#!/usr/bin/env bash

#title           :reset.sh
#description     :Utility script used to remove .txt, .tmp, .zip files in deploy directories 
#author          :Derrick Lord
#date            :20190909
#version         :0.1   
#usage           :./reset.sh
#notes           :Script requires entry in sudoers to restrict password prompt
#notes           :[username]     ALL = (ALL) NOPASSWD:ALL
#notes           :[username] should have R/W access to the following directories:
#notes           :    1. /data1/deploy
#notes           :    2. /mnt/deploy
#notes           :    3. /var/config
#bash_version    :GNU bash 3.2.57(1)-release
#==============================================================================

DEPLOY1_SRC="/data1/deploy/xyz.txt"
DEPLOY2_SRC="/data1/deploy/abc.tmp"
DEPLOY3_SRC="/data1/deploy/svc-config.txt"
DEPLOY1_DEST="/mnt/deploy1/xyz.txt"
DEPLOY2_DEST="/tmp/abc.tmp"
DEPLOY3_DEST="/var/config/svc-config.txt"


function reset_deploy(){
	if [[ -f $DEPLOY1_SRC ]]; then
		(sudo rm -f $DEPLOY1_SRC)
	fi

	if [[ -f $DEPLOY2_SRC ]]; then
		(sudo rm -f $DEPLOY2_SRC)
	fi

	if [[ -f $DEPLOY3_SRC ]]; then
		(sudo rm -f $DEPLOY3_SRC)
	fi

	if [[ -f $DEPLOY1_DEST ]]; then
		(sudo rm -f $DEPLOY1_DEST)
	fi

	if [[ -f $DEPLOY2_DEST ]]; then
		(sudo rm -f $DEPLOY2_DEST)
	fi

	if [[ -f $DEPLOY3_DEST ]]; then
		(sudo rm -f $DEPLOY3_DEST)
	fi
}

reset_deploy


