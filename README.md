# Code Sample #

## Script Setup ##

Script requires entry in sudoers to restrict password prompt
[username]     ALL = (ALL) NOPASSWD:ALL

[username] should have R/W access to the following existing (created) directories:

    1. /data1/deploy

    2. /mnt/deploy

    3. /var/config

## Cron Entries (Test System): ##

10,25,40,55 * * * * /Users/derricklord/Desktop/dev/bash/webshield/deploy.sh >> /tmp/deploy.log

0,15,30,45 * * * * /Users/derricklord/Desktop/dev/bash/webshield/hourly.sh >> /tmp/cron.log

Note: to speed up testing I had the deploy and hourly scripts run every 15 minutes

## Utility Scripts ##

1. reset.sh - utility script used to remove .txt, .tmp, .zip files in deploy directories
2. deploy.sh - Utility script used to deploy a random named file to /data1/deploy
3. hourly.sh - This script will execute hourly via cron to deploy config files